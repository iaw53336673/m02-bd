
## Básicos

### 1. Viviendas  
Diseñar un esquema E/R que recoja la organización de un sistema de información en el que se quiere tener la información sobre municipios, viviendas y personas. Cada persona sólo puede habitar en una vivienda, pero puede ser propietaria de más de una. También nos interesa la interrelación de las personas con su cabeza de familia. (Hacer los supuestos semánticos complementarios necesarios).  

---
### 2. Carreteras
Diseñar una base de datos que contenga información relativa a todas las carreteras de un determinado país. Se pide realizar el diseño en el modelo E/R, sabiendo que:  
>En dicho paıs las carreteras se encuentran divididas en tramos.
>Un tramo siempre pertenece a una única carretera y no puede cambiar de carretera.
>Un tramo puede pasar por varios términos municipales, siendo un dato de interés el km. del tramo por el que entra en dicho término municipal y el km. por el que sale.
>Existen una serie de áreas en las que se agrupan los tramos, cada uno de los cuales no puede pertenecer a más de un área.
---
### 3. Cátedras de Universidad
Diseñar una base de datos que recoja la organización de una Universidad. Se considera que:
>Los departamentos pueden estar en una sola facultad o ser interfacultativos,
agrupando en este caso cátedras que pertenecen a facultades distintas.
>Una cátedra se encuentra en un único departamento.
>Una cátedra pertenece a una sola facultad.
>Un profesor está siempre asignado a un único departamento y adscrito a una o varias cátedras, pudiendo cambiar de cátedra, pero no de departamento. Interesa la fecha en que un profesor es adscrito a una cátedra.
>Existen áreas de conocimiento, y todo departamento tendrá una única área de conocimiento.
---

### 4. Docencia
Se desea diseñar una base de datos para una Universidad que contenga información sobre los Alumnos, las Asignaturas y los Profesores. Construir un modelo E/R teniendo en cuenta las siguientes restricciones:
Una asignatura puede estar impartida por muchos profesores (no a la vez) ya que pueden existir grupos. Un profesor puede dar clases de muchas asignaturas.
Un alumno puede estar matriculado en muchas asignaturas.
Se necesita tener constancia de las asignaturas en las que está matriculado un
alumno, la nota obtenida y el profesor que le ha calificado.
También es necesario tener constancia de las asignaturas que imparten todos los profesores (independientemente de si tienen algún alumno matriculado en su grupo).
No existen asignaturas con el mismo nombre.
Un alumno no puede estar matriculado en la misma asignatura con dos profesores distint 8. os.
---
### 5. Sucursal bancaria
Se desea diseñar una base de datos para una sucursal bancaria que contenga información sobre los clientes, las cuentas, las sucursales y las transacciones producidas. Construir el modelo E/R teniendo en cuenta las siguientes restricciones:
Una transacción viene determinada por su número de transacción, la fecha y la cantidad.
Un cliente puede tener muchas cuentes.
Una cuenta puede tener muchos clientes.
Una cuenta sólo puede estar en una sucursal
---
### 6. Metro de Barcelona
Construir el modelo E/R que refleje toda la información necesaria para la gestión de las líneas de metro de una determinada ciudad. los supuestos semánticos considerados son los siguientes:
Una línea está compuesta por una serie de estaciones en un orden determinado,siendo muy importante recoger la información de ese orden.
Cada estación pertenece al menos a una línea, pudiendo pertenecer a varias.
Una estación nunca deja de pertenecer a una línea a la que anteriormente pertenecıa (p. ej., Portazgo, que pertenece a la línea 1, nunca podrá dejar de pertenecer a esta línea).
Cada estación puede tener varios accesos, pero consideramos que un acceso sólo puede pertenecer a una estación.
Un acceso nunca podrá cambiar de estación.
Cada línea tiene asignados una serie de trenes, no pudiendo suceder que un tren esté asignado a más de una línea, pero sí que no esté asignado a ninguna (p. ej., si se encuentra en reparación)
Algunas estaciones tienen asignadas cocheras, y cada tren tiene asignada una cochera.
Interesa conocer todos los accesos de cada línea.
---
